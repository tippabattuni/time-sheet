## Introduction
This is a sample project to demonstrate using hibernate with SQLite

## References
The example is from this well written blog
https://vrtoonjava.wordpress.com/2012/06/17/shall-we-do-some-spring-together/

and updated to use SQLite using the SQLiteDialect from
https://code.google.com/p/hibernate-sqlite/ with minor updates
